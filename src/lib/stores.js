import { writable } from "svelte/store";

let voucher_store = writable([]);
// let voucher_array_store = writable([]);
let voucher_array_store = writable([]);

voucher_array_store.set([
    {
"id": "1",
"redeemable": "Purchases from Adia's coconut shop",
"offerer": "Adia Kamau",
"name": "Adia Coconuts",
"symbol": "ACO",
"unit": "3.50",
"denomination": "KSh",
"amount": "10000",
"validated": "Validated"
},
{
"id": "2",
"redeemable": "1 regular meal from Sally's restaurant",
"offerer": "Sally Kamau",
"name": "Meal at Sally's restaurant",
"symbol": "MEAL",
"unit": "2.50",
"denomination": "KSh",
"amount": "100000",
"validated": "Validated"
},   
{
"id": "3",
"redeemable": "1 kikapu basket of vegetables",
"offerer": "Vegetable Farmers Organizations",
"name": "Kikapu Vegetables",
"symbol": "KIKAPU",
"unit": "10",
"denomination": "L of water",
"amount": "1000",
"validated": "Validated"
}, 
{
"id": "4",
"redeemable": "Services from Sensorica Lab",
"offerer": "Sensorica Labs",
"name": "Sensorica Vouchers",
"symbol": "SENSORICA",
"unit": "1.00",
"denomination": "CAD",
"amount": "1000",
"validated": "Validated"
},  
]);


export { voucher_store, voucher_array_store };

// user_store.subscribe((value) => localStorage.setItem(user, value));